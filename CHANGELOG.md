This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data.transfer.data-transfer-service

## [v2.4.6] 2020-09-03 
-Restricting geonentwork range

## [v2.4.5] 2020-09-03 
-Change in repository hostname

## [v2.4.4] 2020-09-03

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)